package cap1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    public Connection getConnection() {
        try {
            Connection connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://localhost/meubanco",
                            "root",
                            "123");
            return connection;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
