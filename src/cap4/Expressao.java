package cap4;

import cap5.Visitor;

public interface Expressao {
    int avalia();

    void aceita(Visitor visitor);
}
