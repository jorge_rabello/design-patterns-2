package cap4;

import cap5.Visitor;

public class Numero implements Expressao {

    private final int numero;

    public Numero(int numero) {
        this.numero = numero;
    }

    @Override
    public int avalia() {
        return this.numero;
    }

    public int getNumero() {
        return this.numero;
    }

    @Override
    public void aceita(Visitor impressoraVisitor) {
        impressoraVisitor.visitaNumero(this);
    }
}
