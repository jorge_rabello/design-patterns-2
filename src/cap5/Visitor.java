package cap5;

import cap4.Numero;
import cap4.Soma;
import cap4.Subtracao;

public interface Visitor {

    void visitaSoma(Soma soma);

    void visitaSubtracao(Subtracao subtracao);

    void visitaNumero(Numero numero);
}
