package cap8;

public class Cobranca {

    private Tipo tipo;
    private Fatura fatura;

    public Cobranca() {
    }

    public Cobranca(Tipo tipo, Fatura fatura) {
        this.tipo = tipo;
        this.fatura = fatura;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public Fatura getFatura() {
        return fatura;
    }

    public void emite() {
        System.out.println("Emitido");
    }
}
