package cap8;

public class ContatoCliente {

    private final Cliente cliente;
    private final Cobranca cobranca;

    public ContatoCliente(Cliente cliente, Cobranca cobranca) {
        this.cliente = cliente;
        this.cobranca = cobranca;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Cobranca getCobranca() {
        return cobranca;
    }

    public void dispara() {
        System.out.println("Disparado contato");
    }
}
