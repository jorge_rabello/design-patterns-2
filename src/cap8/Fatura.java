package cap8;

public class Fatura {

    private Cliente cliente;
    private double valor;

    public Fatura() {
    }

    public Fatura(Cliente cliente, double valor) {
        this.cliente = cliente;
        this.valor = valor;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public double getValor() {
        return valor;
    }
}
