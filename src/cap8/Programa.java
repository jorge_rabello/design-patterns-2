package cap8;

public class Programa {
    public static void main(String[] args) {

        EmpresaFacade facade = new EmpresaFacadeSingleton().getInstancia();
        String cpf = "1223";
        facade.buscaCliente(cpf);
        facade.criaFatura(new Cliente(), 12233.0);

    }
}
